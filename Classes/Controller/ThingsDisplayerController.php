<?php
namespace Fmontalbano\Typo3PrettyExtension\Controller;

use Fmontalbano\Campfire\Utility\CampfireUtility;

/***
 *
 * This file is part of the "typo-3-pretty-extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Florian
 *
 ***/

/**
 * ThingsDisplayerController
 */
class ThingsDisplayerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action displayTitle
     * 
     * @return void
     */
    public function displayTitleAction()
    {

    }

    /**
     * action haveFun
     * 
     * @return void
     */
    public function haveFunAction()
    {
        $displayWoodImage = CampfireUtility::checkIfImageExists("swedishFireTorch.jpg");
        $displayMatchstickImage = CampfireUtility::checkIfImageExists("matchstick.jpg");

        $this->view->assign("displayWood", $displayWoodImage);
        $this->view->assign("displayMatchstick", $displayMatchstickImage);
    }
}
