<?php
namespace Fmontalbano\Typo3PrettyExtension\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Florian 
 */
class ThingsDisplayerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Fmontalbano\Typo3PrettyExtension\Domain\Model\ThingsDisplayer
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Fmontalbano\Typo3PrettyExtension\Domain\Model\ThingsDisplayer();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
