<?php
namespace Fmontalbano\Typo3PrettyExtension\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Florian 
 */
class ThingsDisplayerControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Fmontalbano\Typo3PrettyExtension\Controller\ThingsDisplayerController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Fmontalbano\Typo3PrettyExtension\Controller\ThingsDisplayerController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
