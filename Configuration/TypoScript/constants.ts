
plugin.tx_typo3prettyextension_thingsdisplayer {
    view {
        # cat=plugin.tx_typo3prettyextension_thingsdisplayer/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:typo_3_pretty_extension/Resources/Private/Templates/
        # cat=plugin.tx_typo3prettyextension_thingsdisplayer/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:typo_3_pretty_extension/Resources/Private/Partials/
        # cat=plugin.tx_typo3prettyextension_thingsdisplayer/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:typo_3_pretty_extension/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_typo3prettyextension_thingsdisplayer//a; type=string; label=Default storage PID
        storagePid =
    }
}
